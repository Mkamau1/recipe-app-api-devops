#This files stores the variables for terraform. The variables give us the ability to reference them somewhere else.
#The variable below adds a prefix to our resources
variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "morris.kamau@opencastsoftware.com"
}

variable "db_username" {
  description = "Username for the RDS postgres Instance"
}

variable "db_password" {
  description = "Password for the RDS postgres Instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devop-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "956163692607.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "956163692607.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django App"
}