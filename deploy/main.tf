terraform {
  backend "s3" {
    bucket         = "recipe-app-devops-feature-123455"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.50.0"
}

#Using the variable called prefix on ./variables.tf 
#The var.prefix below extracts the value. 
#The terrafrom.workspace extracts the name of the workspace being used by terraform.
#This is then used in other files such as ./bastion.tf to create a prefix for naming the resources.
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
#This gives a resource that we can use to retrive the current region that our terraform is going to deploy to. 
#This is done in order to avoid us having to hardcode the region elsewhere.
data "aws_region" "current" {}